import { useState } from 'react';

const useCheckboxesSelect = (defaultValue,categories,validateValueFunction) => {
  const [enteredValue, setEnteredValue] = useState(defaultValue);
  const [isTouched, setIsTouched] = useState(false);
  const [selectAll, setSelectAll] = useState(false);

  const valueIsValid = validateValueFunction(enteredValue);
  const hasError = !valueIsValid && isTouched;

  const valueChangeHandler = (event) => {
    const {
        target: { value },
      } = event;
  
      if (event.target.value.includes("Select all")) {
        if (!selectAll) {
          setSelectAll(true);
          setEnteredValue(categories);
        } else {
          setSelectAll(false);
          setEnteredValue([]);
        }
      } else {
        if (selectAll) setSelectAll(false);
        setEnteredValue(value);
      }
  };

  const inputBlurHandler = () => {
    setIsTouched(true);
  };

  const reset = () => {
    setEnteredValue(defaultValue);
    setIsTouched(false);
  };

  return {
    value: enteredValue,
    isValid: valueIsValid,
    selectAll,
    hasError,
    valueChangeHandler,
    inputBlurHandler,
    reset
  };
};

export default useCheckboxesSelect;