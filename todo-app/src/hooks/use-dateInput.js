import { useState } from 'react';

const useDateInput = (defaultValue,validateValueFunction) => {
  const [enteredValue, setEnteredValue] = useState(defaultValue);
  const [isTouched, setIsTouched] = useState(false);

  const valueIsValid = validateValueFunction(enteredValue);
  const hasError = !valueIsValid && isTouched;

  let todayDate = new Date();
  todayDate.setDate(todayDate.getDate()-2);
  const minVal=new Date(todayDate.toISOString().slice(0,10))

  const valueChangeHandler = (value) => {
    setEnteredValue(value);
  };

  const inputBlurHandler = () => {
    setIsTouched(true);
  };

  const reset = () => {
    setEnteredValue(defaultValue);
    setIsTouched(false);
  };

  return {
    value: enteredValue,
    isValid: valueIsValid,
    hasError,
    valueChangeHandler,
    inputBlurHandler,
    reset,
    minVal
  };
};

export default useDateInput;