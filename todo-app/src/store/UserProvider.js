import * as React from "react";
import { useState } from "react";
import UserContext from "./UserContext";
import { v4 as uuid } from "uuid";
import authenticateSubject from "subjects/authenticateSubject";

const UserProvider = (props) => {
  const defaultUser = {
    id: "",
    email: "",
    firstName: "",
    lastName: "",
    gender: "",
    address: "",
    password: "",
    profileImage: "",
    tasksAddress: "",
  };
  const [user, setUser] = useState(defaultUser);
  const [userIsLoggedIn, setUserIsLoggedIn] = useState(false);

  const addNewUserHandler = (
    email,
    firstName,
    lastName,
    gender,
    address,
    password
  ) => {
    const data = {
      email: email.value,
      firstName: firstName.value,
      lastName: lastName.value,
      gender: gender.value,
      address: address.value,
      password: password.value,
    };

    const entriesAreValid =
      email.isValid &&
      firstName.isValid &&
      lastName.isValid &&
      gender.isValid &&
      address.isValid &&
      password.isValid;

    const emailIsUnique = emailDoesNotExistInDatabase(email.value);

    if (!entriesAreValid) {
      return {
        status: "Error",
        message: "Signup details have incorrect format",
        helperMessage: "Please enter details in correct format",
        formValues: data,
        user: null,
      };
    } else if (!emailIsUnique) {
      return {
        status: "Error",
        message: "Entered email already exists",
        helperMessage: "Please enter diffrent email Id or switch to Sign In",
        formValues: data,
        user: null,
      };
    } else {
      const userUniqueId = uuid();
      const tasksAddressUniqueId = uuid();
      const newUser = {
        id: userUniqueId,
        ...data,
        profileImage: "",
        tasksAddress: tasksAddressUniqueId,
      };
      editUsersDatabase(newUser, false);
      authenticateSubject.next({ tasksAddress: newUser.tasksAddress });
      setUser(newUser);
      setUserIsLoggedIn(true);
      return {
        status: "Ok",
        message: "Signup is successfull",
        user: newUser,
      };
    }
  };

  const verifySigninCredentialsHandler = (email, password) => {
    const entriesAreValid = email.isValid && password.isValid;

    if (!entriesAreValid)
      return {
        status: "Error",
        message: "Signin credentials have incorrect format",
        helperMessage: "Please enter email and password in correct format",
        formValues: {
          email: email.value,
          password: password.value,
        },
        user: null,
      };

    const { isExisting, userExtracted } = userExistsInDatabase(
      email.value,
      password.value
    );

    if (isExisting) {
      setUser(userExtracted);
      setUserIsLoggedIn(true);
      authenticateSubject.next({ tasksAddress: userExtracted.tasksAddress });
      return {
        status: "Ok",
        message: "Signin credentials are correct",
        user: userExtracted,
      };
    } else {
      return {
        status: "Error",
        message: "Signin credentials are wrong.",
        helperMessage: "Please enter correct email and password.",
        formValues: {
          email: email.value,
          password: password.value,
        },
        user: null,
      };
    }
  };

  const updateUserHandler = (email, firstName, lastName, gender, address) => {
    const data = {
      email: email.value,
      firstName: firstName.value,
      lastName: lastName.value,
      gender: gender.value,
      address: address.value,
    };

    const entriesAreValid =
      email.isValid &&
      firstName.isValid &&
      lastName.isValid &&
      gender.isValid &&
      address.isValid;

    const emailIsUnique = emailDoesNotExistInDatabase(email.value, user.id);

    if (!entriesAreValid) {
      return {
        status: "Error",
        message: "Profile details have incorrect format",
        helperMessage: "Please enter details in correct format",
        formValues: data,
        user: null,
      };
    } else if (!emailIsUnique) {
      return {
        status: "Error",
        message: "Entered email already exists",
        helperMessage: "Please enter diffrent email Id",
        formValues: data,
        user: null,
      };
    } else {
      let edittedUser;
      setUser((prevState) => {
        edittedUser = { ...prevState, ...data };
        return edittedUser;
      });
      editUsersDatabase(edittedUser, true);
      return {
        status: "Ok",
        message: "Profile is successfully editted",
        user: edittedUser,
      };
    }
  };

  const updateProfileImageHandler = (img) => {
    let edittedUser;
    setUser((prevState) => {
      edittedUser = { ...prevState, profileImage: img };
      return edittedUser;
    });
    editUsersDatabase(edittedUser, true);
  };

  const logoutUserHandler = () => {
    setUser(defaultUser);
    setUserIsLoggedIn(false);
  };

  const emailDoesNotExistInDatabase = (email, id) => {
    const Users = JSON.parse(localStorage.getItem("Users"));
    let emailDoesNotExists = true;
    Users.forEach((elem) => {
      console.log(elem, email);
      if (id !== undefined && elem.id === id) return;
      if (elem.email === email) emailDoesNotExists = false;
    });
    console.log(emailDoesNotExists);
    return emailDoesNotExists;
  };

  const userExistsInDatabase = (email, password) => {
    const Users = JSON.parse(localStorage.getItem("Users"));
    let userExists = false;
    let existingUser = null;
    Users.forEach((elem) => {
      if (elem.email === email && elem.password === password) {
        userExists = true;
        existingUser = elem;
      }
    });
    return { isExisting: userExists, userExtracted: existingUser };
  };

  const editUsersDatabase = (user, editMode) => {
    const Users = JSON.parse(localStorage.getItem("Users"));
    if (!editMode) Users.push(user);
    else {
      Users.forEach((elem, index) => {
        if (elem.id === user.id) {
          Users[index] = user;
        }
      });
    }
    localStorage.setItem("Users", JSON.stringify(Users));
  };

  const userContext = {
    user: user,
    userIsLoggedIn: userIsLoggedIn,
    addNewUser: addNewUserHandler,
    verifySigninCredentials: verifySigninCredentialsHandler,
    updateUser: updateUserHandler,
    updateProfileImage: updateProfileImageHandler,
    logoutUser: logoutUserHandler,
  };

  return (
    <UserContext.Provider value={userContext}>
      {props.children}
    </UserContext.Provider>
  );
};

export default UserProvider;
