import * as React from "react";
import { useEffect, useState } from "react";
import TaskContext from "./TaskContext";
import { v4 as uuid } from "uuid";
import authenticateSubject from "subjects/authenticateSubject";

const TaskProvider = (props) => {
  const [tasks, setTasks] = useState([]);
  const [tasksToDisplay, setTasksToDisplay] = useState(tasks);
  const [tasksAddress, setTaskAddress] = useState("");

  const [deleteMode, setDeleteMode] = useState(false);
  const [deleteList, setDeleteList] = useState([]);

  const defaultFilter = {
    taskTypes: [],
    taskCategories: [],
    startDate: null,
    endDate: null,
    sortType: "",
  };
  const [loadedFilter, setLoadedFilter] = useState(defaultFilter);

  useEffect(() => {
    setDeleteMode(false);
    setDeleteList([]);
    const filterTasksHandler = (
      taskTypes,
      taskCategories,
      startDate,
      endDate,
      sortType
    ) => {
      setTasksToDisplay(tasks);

      if (taskTypes.length !== 0) filterTasksByTaskTypes(taskTypes);
      if (taskCategories.length !== 0)
        filterTasksByTaskCategories(taskCategories);
      if (startDate !== null) filterTasksByStartDate(startDate);
      if (endDate !== null) filterTasksByEndDate(endDate);
      if (sortType !== "") sortTasks(sortType);
    };

    const filterTasksByTaskTypes = (taskTypes) => {
      setTasksToDisplay((prevState) => {
        return prevState.filter((elem) => {
          if (elem.doneStatus === true && taskTypes.includes("Completed Tasks"))
            return true;
          else if (
            new Date(elem.dueDate) >= new Date() &&
            taskTypes.includes("Due Tasks")
          )
            return true;
          else if (
            new Date(elem.dueDate) < new Date() &&
            taskTypes.includes("Overdue Tasks")
          )
            return true;
          else return false;
        });
      });
    };

    const filterTasksByTaskCategories = (taskCategories) => {
      setTasksToDisplay((prevState) => {
        return prevState.filter((elem) => {
          let ans = false;
          elem.taskCategories.forEach((category) => {
            ans = ans || taskCategories.includes(category);
          });
          return ans;
        });
      });
    };

    const filterTasksByStartDate = (startDate) => {
      setTasksToDisplay((prevState) => {
        return prevState.filter((elem) => {
          if (new Date(elem.dueDate) >= new Date(startDate)) return true;
          else return false;
        });
      });
    };

    const filterTasksByEndDate = (endDate) => {
      setTasksToDisplay((prevState) => {
        return prevState.filter((elem) => {
          if (new Date(elem.dueDate) <= new Date(endDate)) return true;
          else return false;
        });
      });
    };

    const sortTasks = (sortType) => {
      setTasksToDisplay((prevState) => {
        let tempArray = [...prevState];
        if (sortType === "Sort by name (Asc)") {
          tempArray.sort((a, b) => {
            return a.taskName > b.taskName ? 1 : -1;
          });
        } else if (sortType === "Sort by name (Desc)") {
          tempArray.sort((a, b) => {
            return a.taskName > b.taskName ? -1 : 1;
          });
        } else if (sortType === "Sort by due date (Asc)") {
          tempArray.sort((a, b) => {
            return new Date(a.dueDate) > new Date(b.dueDate) ? 1 : -1;
          });
        } else {
          tempArray.sort((a, b) => {
            return new Date(a.dueDate) > new Date(b.dueDate) ? -1 : 1;
          });
        }
        return tempArray;
      });
    };

    filterTasksHandler(
      loadedFilter.taskTypes,
      loadedFilter.taskCategories,
      loadedFilter.startDate,
      loadedFilter.endDate,
      loadedFilter.sortType
    );
  }, [tasks, loadedFilter]);

  authenticateSubject.subscribe((data) => {
    setTaskAddress(data.tasksAddress);
    if (localStorage.getItem(data.tasksAddress) === null)
      localStorage.setItem(data.tasksAddress, JSON.stringify([]));
    else {
      const tasksExtracted = JSON.parse(
        localStorage.getItem(data.tasksAddress)
      );
      setTasks(tasksExtracted);
      setTasksToDisplay(tasksExtracted);
    }
  });

  const addNewTaskHandler = (
    taskName,
    dueDate,
    taskCategories,
    taskImage,
    publicStatus,
    doneStatus
  ) => {
    const data = {
      taskName: taskName.value,
      dueDate: dueDate.value,
      taskCategories: taskCategories.value,
      taskImage: taskImage.value,
      publicStatus: publicStatus.value,
      doneStatus: doneStatus.value,
    };

    const entriesAreValid =
      taskName.isValid && dueDate.isValid && taskCategories.isValid;

    if (!entriesAreValid) {
      return {
        status: "Error",
        message: "Task details have incorrect format",
        helperMessage: "Please enter details in correct format",
        formValues: data,
        task: null,
      };
    } else {
      const unique_id = uuid();
      let edittedTasks;
      setTasks((prevState) => {
        edittedTasks = [...prevState, { id: unique_id, ...data }];
        return edittedTasks;
      });

      editTasksInDatabase(edittedTasks);
      return {
        status: "Ok",
        message: "Task is successfully added",
        tasks: edittedTasks,
      };
    }
  };

  const updateTaskHandler = (
    id,
    taskName,
    dueDate,
    taskCategories,
    taskImage,
    publicStatus,
    doneStatus
  ) => {
    const data = {
      id: id.value,
      taskName: taskName.value,
      dueDate: dueDate.value,
      taskCategories: taskCategories.value,
      taskImage: taskImage.value,
      publicStatus: publicStatus.value,
      doneStatus: doneStatus.value,
    };

    const entriesAreValid =
      taskName.isValid && dueDate.isValid && taskCategories.isValid;

    if (!entriesAreValid) {
      return {
        status: "Error",
        message: "Task details have incorrect format",
        helperMessage: "Please enter details in correct format",
        formValues: data,
        task: null,
      };
    } else {
      let edittedTasks;
      setTasks((prevState) => {
        edittedTasks = [...prevState];
        edittedTasks.forEach((task, index) => {
          if (task.id === data.id) {
            edittedTasks[index] = data;
          }
        });
        return edittedTasks;
      });
      editTasksInDatabase(edittedTasks);
      return {
        status: "Ok",
        message: "Task is successfully editted",
        tasks: edittedTasks,
      };
    }
  };

  const applyFilterHandler = (
    taskTypes,
    taskCategories,
    startDate,
    endDate,
    sortType
  ) => {
    setLoadedFilter({
      taskTypes: taskTypes,
      taskCategories: taskCategories,
      startDate: startDate,
      endDate: endDate,
      sortType: sortType,
    });
  };

  const setDeleteModeHandler = () => {
    setDeleteMode(true);
  };

  const resetDeleteModeHandler = () => {
    setDeleteMode(false);
  };

  const addTaskToDeleteListHandler = (task) => {
    setDeleteList((prevState) => {
      return [...prevState, task];
    });
  };

  const removeTaskFromDeleteListHandler = (task) => {
    let indexToRemove;
    deleteList.forEach((elem, index) => {
      if (elem.id === task.id) {
        indexToRemove = index;
      }
    });
    setDeleteList((prevState) => {
      prevState.splice(indexToRemove, 1);
      return prevState;
    });
  };

  const resetDeleteListHandler = () => {
    setDeleteList([]);
  };

  const deleteTasksHandler = () => {
    let edittedTasks;
    setTasks((prevState) => {
      edittedTasks = prevState.filter((elem) => {
        let elemPresentInDelList = false;
        deleteList.forEach((delListElem) => {
          if (delListElem.id === elem.id) {
            elemPresentInDelList = true;
          }
        });
        return !elemPresentInDelList;
      });
      return edittedTasks;
    });
    editTasksInDatabase(edittedTasks);
    return {
      status: "Ok",
      message: "Seleted tasks are successfully deleted",
      tasks: edittedTasks,
    };
  };

  const resetTasksToDisplayHandler = () => {
    setLoadedFilter(defaultFilter);
  };

  const editTasksInDatabase = (tasks) => {
    localStorage.setItem(tasksAddress, JSON.stringify(tasks));
  };

  const taskContext = {
    tasks: tasks,
    tasksToDisplay: tasksToDisplay,
    resetTasksToDisplay: resetTasksToDisplayHandler,

    addNewTask: addNewTaskHandler,
    updateTask: updateTaskHandler,
    applyFilter: applyFilterHandler,

    deleteMode: deleteMode,
    deleteList: deleteList,
    setDeleteMode: setDeleteModeHandler,
    resetDeleteMode: resetDeleteModeHandler,
    addTaskToDeleteList: addTaskToDeleteListHandler,
    removeTaskFromDeleteList: removeTaskFromDeleteListHandler,
    resetDeleteList: resetDeleteListHandler,
    deleteTasks: deleteTasksHandler,
  };

  return (
    <TaskContext.Provider value={taskContext}>
      {props.children}
    </TaskContext.Provider>
  );
};

export default TaskProvider;
