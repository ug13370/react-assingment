import { Avatar, Button, Card, Checkbox, Grid, Stack } from "@mui/material";
import EditTaskModal from "modals/EditTaskModal";
import React, { useContext, useEffect, useRef, useState } from "react";
import { Fragment } from "react";
import AssignmentIcon from "@mui/icons-material/Assignment";
import TaskContext from "store/TaskContext";

const TaskItem = (props) => {
  const taskCtx = useContext(TaskContext);
  const task = props.task;

  const defaultEditTaskModal = { state: false, data: {} };
  const [editTaskModal, setEditTaskModal] = useState(defaultEditTaskModal);
  const closeEditTaskModal = () => {
    setEditTaskModal(defaultEditTaskModal);
  };
  const editTask = () => {
    setEditTaskModal((prevState) => {
      return {
        ...prevState,
        state: true,
        data: { editMode: true, task: task },
      };
    });
  };

  const [deleteItemCheckbox, setDeleteItemCheckbox] = useState(false);
  useEffect(() => {
    if (!taskCtx.deleteMode) {
      setDeleteItemCheckbox(false);
    }
  }, [taskCtx.deleteMode]);

  const difference = (date1, date2) => {
    const date1utc = Date.UTC(
      date1.getFullYear(),
      date1.getMonth(),
      date1.getDate()
    );
    const date2utc = Date.UTC(
      date2.getFullYear(),
      date2.getMonth(),
      date2.getDate()
    );
    const day = 1000 * 60 * 60 * 24;
    return (date2utc - date1utc) / day;
  };

  const dueDaysString = () => {
    const dueDate = task.dueDate;
    const doneStatus = task.doneStatus;

    if (doneStatus) return "Completed";
    else {
      if (new Date(dueDate) > new Date()) {
        return difference(new Date(), new Date(dueDate)) + " days due";
      } else {
        return difference(new Date(dueDate), new Date()) + " days overdue";
      }
    }
  };

  const markAsDone = (event) => {
    event.preventDefault();

    const response = taskCtx.updateTask(
      { value: task.id },
      {
        value: task.taskName,
        isValid: true,
      },
      {
        value: task.dueDate,
        isValid: true,
      },
      {
        value: task.taskCategories,
        isValid: true,
      },
      {
        value: task.taskImage,
      },
      {
        value: task.publicStatus,
      },
      {
        value: true,
      }
    );
    console.log(response);
  };

  const label = { inputProps: { "aria-label": "Checkbox demo" } };
  const deleteCheckboxHandler = (event) => {
    setDeleteItemCheckbox(event.target.checked);
    if (event.target.checked) {
      taskCtx.addTaskToDeleteList(task);
    } else {
      taskCtx.removeTaskFromDeleteList(task);
    }
    console.log(taskCtx.deleteList);
  };

  return (
    <Fragment>
      {editTaskModal.state && (
        <EditTaskModal
          closeModal={closeEditTaskModal}
          openModal={editTaskModal}
        ></EditTaskModal>
      )}
      <Card style={{ padding: "3%" }}>
        <Grid container>
          <Grid item xs={9}>
            <Stack
              direction="column"
              justifyContent="center"
              alignItems="stretch"
              spacing={1}
            >
              <Stack
                direction="column"
                justifyContent="center"
                alignItems="stretch"
                spacing={-2}
              >
                <h3>
                  {taskCtx.deleteMode && (
                    <Checkbox
                      {...label}
                      checked={deleteItemCheckbox}
                      onChange={deleteCheckboxHandler}
                      sx={{
                        "& .MuiSvgIcon-root": { fontSize: 28 },
                        paddingLeft: "0%",
                      }}
                    />
                  )}
                  {task.taskName} ({dueDaysString()})
                </h3>
                <small>
                  {task.publicStatus ? "[Public Task]" : "[Private Task]"}
                </small>
              </Stack>

              <Stack
                direction="column"
                justifyContent="center"
                alignItems="stretch"
                spacing={-2}
              >
                <p>
                  Categories :-
                  {task.taskCategories.map((category) => {
                    return category + ", ";
                  })}
                </p>
                <small>
                  Due date :- {new Date(task.dueDate).toLocaleDateString()}
                </small>
              </Stack>

              <Stack
                direction="row"
                justifyContent="flex-start"
                alignItems="center"
                spacing={1}
              >
                <Button
                  variant="contained"
                  style={{
                    backgroundColor: "#004DC8",
                    color: "#ffffff",
                  }}
                  onClick={editTask}
                >
                  Edit Task
                </Button>

                {!task.doneStatus && (
                  <Button
                    variant="outlined"
                    style={{
                      color: "#004DC8",
                    }}
                    onClick={markAsDone}
                  >
                    Mark as done
                  </Button>
                )}
              </Stack>
            </Stack>
          </Grid>
          <Grid
            item
            xs={3}
            container
            direction="row"
            justifyContent="center"
            alignItems="center"
          >
            <Avatar
              sx={{ bgcolor: "#0077E2", width: 100, height: 100 }}
              variant="rounded"
              src={task.taskImage}
            >
              {task.taskImage === null && (
                <AssignmentIcon sx={{ width: 80, height: 80 }}></AssignmentIcon>
              )}
            </Avatar>
          </Grid>
        </Grid>
      </Card>
    </Fragment>
  );
};
export default TaskItem;
