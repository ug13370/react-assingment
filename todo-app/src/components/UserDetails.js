import {
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Button,
  Stack,
} from "@mui/material";
import useInput from "hooks/use-input";
import ErrorModal from "modals/ErrorModal";
import React, { Fragment, useContext, useState } from "react";
import UserContext from "store/UserContext";

const UserDetails = () => {
  const userCtx = useContext(UserContext);
  const user = userCtx.user;

  const [editMode, setEditMode] = useState(false);

  const defaultProfileModal = { state: false, data: {} };
  const [profileModal, setProfileModal] = useState(defaultProfileModal);
  const closeProfileModal = () => {
    setProfileModal(defaultProfileModal);
  };

  const {
    value: enteredEmail,
    isValid: enteredEmailIsValid,
    hasError: emailInputHasError,
    valueChangeHandler: emailChangeHandler,
    inputBlurHandler: emailBlurHandler,
    reset: emailResetHandler,
  } = useInput(user.email, (value) => {
    return value.includes("@zs.com");
  });

  const {
    value: enteredFirstName,
    isValid: enteredFirstNameIsValid,
    hasError: firstNameInputHasError,
    valueChangeHandler: firstNameChangeHandler,
    inputBlurHandler: firstNameBlurHandler,
    reset: firstNameResetHandler,
  } = useInput(user.firstName, (value) => {
    return true;
  });

  const {
    value: enteredLastName,
    isValid: enteredLastNameIsValid,
    hasError: lastNameInputHasError,
    valueChangeHandler: lastNameChangeHandler,
    inputBlurHandler: lastNameBlurHandler,
    reset: lastNameResetHandler,
  } = useInput(user.lastName, (value) => {
    return true;
  });

  const {
    value: enteredGender,
    isValid: enteredGenderIsValid,
    hasError: genderInputHasError,
    valueChangeHandler: genderChangeHandler,
    inputBlurHandler: genderBlurHandler,
    reset: genderResetHandler,
  } = useInput(user.gender, (value) => {
    return true;
  });

  const {
    value: enteredAddress,
    isValid: enteredAddressIsValid,
    hasError: addressInputHasError,
    valueChangeHandler: addressChangeHandler,
    inputBlurHandler: addressBlurHandler,
    reset: addressResetHandler,
  } = useInput(user.address, (value) => {
    return true;
  });

  const formIsValid =
    enteredEmailIsValid &&
    enteredFirstNameIsValid &&
    enteredLastNameIsValid &&
    enteredGenderIsValid &&
    enteredAddressIsValid;

  const resetForm = () => {
    emailResetHandler();
    firstNameResetHandler();
    lastNameResetHandler();
    genderResetHandler();
    addressResetHandler();
  };

  const editProfile = (event) => {
    event.preventDefault();
    setEditMode(true);
  };

  const saveProfile = (event) => {
    event.preventDefault();

    const response = userCtx.updateUser(
      { entryType: "Email", value: enteredEmail, isValid: enteredEmailIsValid },
      {
        entryType: "FirstName",
        value: enteredFirstName,
        isValid: enteredFirstNameIsValid,
      },
      {
        entryType: "LastName",
        value: enteredLastName,
        isValid: enteredLastNameIsValid,
      },
      {
        entryType: "Gender",
        value: enteredGender,
        isValid: enteredGenderIsValid,
      },
      {
        entryType: "Address",
        value: enteredAddress,
        isValid: enteredAddressIsValid,
      }
    );
    if (response.status === "Error") {
      setProfileModal((prevState) => {
        return { ...prevState, state: true, data: response };
      });
    }

    if (response.status === "Ok") setEditMode(false);

    console.log(response);
  };

  const onCancel = () => {
    resetForm();
    setEditMode(false);
  };

  return (
    <Fragment>
      {profileModal.state && (
        <ErrorModal
          closeModal={closeProfileModal}
          openModal={profileModal}
        ></ErrorModal>
      )}
      <form>
        <Stack
          direction="column"
          justifyContent="center"
          alignItems="stretch"
          spacing={2}
        >
          <h2>User details :-</h2>
          <TextField
            id="email"
            label="Email"
            value={enteredEmail}
            onChange={emailChangeHandler}
            onBlur={emailBlurHandler}
            error={emailInputHasError}
            focused={emailInputHasError}
            helperText={
              emailInputHasError
                ? "Please enter email in format xyz@zs.com"
                : ""
            }
            inputProps={{ readOnly: !editMode }}
            fullWidth
          />
          <Stack
            direction="row"
            justifyContent="center"
            alignItems="center"
            spacing={2}
          >
            <TextField
              id="firstName"
              label="First Name"
              value={enteredFirstName}
              onChange={firstNameChangeHandler}
              onBlur={firstNameBlurHandler}
              error={firstNameInputHasError}
              focused={firstNameInputHasError}
              inputProps={{ readOnly: !editMode }}
              fullWidth
            />
            <TextField
              id="lastName"
              label="Last Name"
              value={enteredLastName}
              onChange={lastNameChangeHandler}
              onBlur={lastNameBlurHandler}
              error={lastNameInputHasError}
              focused={lastNameInputHasError}
              inputProps={{ readOnly: !editMode }}
              fullWidth
            />
          </Stack>
          <FormControl fullWidth>
            <InputLabel id="ageSelectLabel">Gender</InputLabel>
            <Select
              labelId="ageSelectLabel"
              id="ageSelect"
              label="Gender"
              value={enteredGender}
              onChange={genderChangeHandler}
              onBlur={genderBlurHandler}
              error={genderInputHasError}
              inputProps={{ readOnly: !editMode }}
            >
              <MenuItem value={"male"}>Male</MenuItem>
              <MenuItem value={"female"}>Female</MenuItem>
              <MenuItem value={"other"}>Other</MenuItem>
              <MenuItem value={"none"}>None</MenuItem>
            </Select>
          </FormControl>
          <TextField
            id="address"
            label="Address"
            multiline
            rows={2}
            value={enteredAddress}
            onChange={addressChangeHandler}
            onBlur={addressBlurHandler}
            error={addressInputHasError}
            focused={addressInputHasError}
            inputProps={{ readOnly: !editMode }}
            fullWidth
          />
          <Stack
            direction="row"
            justifyContent="flex-start"
            alignItems="center"
            spacing={2}
          >
            {!editMode && (
              <Button
                variant="contained"
                style={{
                  backgroundColor: "#004DC8",
                  color: "#ffffff",
                  marginRight: "1%",
                }}
                onClick={editProfile}
              >
                Edit Proflie
              </Button>
            )}
            {editMode && (
              <Button
                variant="contained"
                style={{
                  backgroundColor: "#004DC8",
                  color: "#ffffff",
                  marginRight: "1%",
                }}
                onClick={saveProfile}
                disabled={!formIsValid}
              >
                Save Profile
              </Button>
            )}
            {editMode && (
              <Button
                variant="outlined"
                style={{
                  color: "#004DC8",
                  marginLeft: "1%",
                }}
                onClick={onCancel}
              >
                Cancel
              </Button>
            )}
          </Stack>
        </Stack>
      </form>
    </Fragment>
  );
};

export default UserDetails;
