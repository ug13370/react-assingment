import { Button, Stack, TextField } from "@mui/material";
import useInput from "hooks/use-input";
import ErrorModal from "modals/ErrorModal";
import React, { Fragment, useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import UserContext from "store/UserContext";

const Signin = () => {
  const userCtx = useContext(UserContext);
  const navigate = useNavigate();

  const defaultSigninModal = { state: false, data: {} };
  const [signinModal, setSigninModal] = useState(defaultSigninModal);
  const closeSigninModal = () => {
    setSigninModal(defaultSigninModal);
  };

  const {
    value: enteredEmail,
    isValid: enteredEmailIsValid,
    hasError: emailInputHasError,
    valueChangeHandler: emailChangeHandler,
    inputBlurHandler: emailBlurHandler,
    reset: emailResetHandler,
  } = useInput("", (value) => {
    return value.includes("@zs.com");
  });

  const {
    value: enteredPassword,
    isValid: enteredPasswordIsValid,
    hasError: passwordInputHasError,
    valueChangeHandler: passwordChangeHandler,
    inputBlurHandler: passwordBlurHandler,
    reset: passwordResetHandler,
  } = useInput("", (value) => {
    return value.length >= 4;
  });

  const formIsValid = enteredEmailIsValid && enteredPasswordIsValid;

  const resetForm = () => {
    emailResetHandler();
    passwordResetHandler();
  };
  const submitHandler = (event) => {
    event.preventDefault();
    const response = userCtx.verifySigninCredentials(
      { entryType: "Email", value: enteredEmail, isValid: enteredEmailIsValid },
      {
        entryType: "Password",
        value: enteredPassword,
        isValid: enteredPasswordIsValid,
      }
    );
    if (response.status === "Error") {
      setSigninModal((prevState) => {
        return { ...prevState, state: true, data: response };
      });
    }

    if (response.status === "Ok") {
      navigate("/tasks");
    }
    console.log(response);
    resetForm();
  };

  return (
    <Fragment>
      {signinModal.state && (
        <ErrorModal
          closeModal={closeSigninModal}
          openModal={signinModal}
        ></ErrorModal>
      )}
      <form onSubmit={submitHandler}>
        <Stack
          direction="column"
          justifyContent="center"
          alignItems="stretch"
          spacing={2}
        >
          <h2>Sign In</h2>

          <TextField
            id="email"
            label="Email"
            value={enteredEmail}
            onChange={emailChangeHandler}
            onBlur={emailBlurHandler}
            error={emailInputHasError}
            focused={emailInputHasError}
            helperText={
              emailInputHasError
                ? "Please enter email in format xyz@zs.com"
                : ""
            }
            fullWidth
          />

          <TextField
            id="password"
            label="Password"
            type="password"
            value={enteredPassword}
            onChange={passwordChangeHandler}
            onBlur={passwordBlurHandler}
            error={passwordInputHasError}
            focused={passwordInputHasError}
            helperText={
              passwordInputHasError
                ? "Password must be atleast 4 characters long"
                : ""
            }
            fullWidth
          ></TextField>

          <Button
            variant="contained"
            style={{
              backgroundColor: "#004DC8",
              color: "#ffffff",
            }}
            disabled={!formIsValid}
            type="submit"
          >
            Sign In
          </Button>
        </Stack>
      </form>
    </Fragment>
  );
};

export default Signin;
