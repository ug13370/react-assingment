import useInput from "hooks/use-input";
import ErrorModal from "modals/ErrorModal";
import { Fragment, useContext, useState } from "react";
import { useNavigate } from "react-router-dom";
import UserContext from "store/UserContext";

const {
  TextField,
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  Button,
  Stack,
} = require("@mui/material");
const React = require("react");

const Signup = () => {
  const userCtx = useContext(UserContext);
  const navigate = useNavigate();

  const defaultSignupModal = { state: false, data: {} };
  const [signupModal, setSignupModal] = useState(defaultSignupModal);
  const closeSignupModal = () => {
    setSignupModal(defaultSignupModal);
  };

  const {
    value: enteredEmail,
    isValid: enteredEmailIsValid,
    hasError: emailInputHasError,
    valueChangeHandler: emailChangeHandler,
    inputBlurHandler: emailBlurHandler,
    reset: emailResetHandler,
  } = useInput("", (value) => {
    return value.includes("@zs.com");
  });

  const {
    value: enteredFirstName,
    isValid: enteredFirstNameIsValid,
    hasError: firstNameInputHasError,
    valueChangeHandler: firstNameChangeHandler,
    inputBlurHandler: firstNameBlurHandler,
    reset: firstNameResetHandler,
  } = useInput("", (value) => {
    return true;
  });

  const {
    value: enteredLastName,
    isValid: enteredLastNameIsValid,
    hasError: lastNameInputHasError,
    valueChangeHandler: lastNameChangeHandler,
    inputBlurHandler: lastNameBlurHandler,
    reset: lastNameResetHandler,
  } = useInput("", (value) => {
    return true;
  });

  const {
    value: enteredGender,
    isValid: enteredGenderIsValid,
    hasError: genderInputHasError,
    valueChangeHandler: genderChangeHandler,
    inputBlurHandler: genderBlurHandler,
    reset: genderResetHandler,
  } = useInput("none", (value) => {
    return true;
  });

  const {
    value: enteredAddress,
    isValid: enteredAddressIsValid,
    hasError: addressInputHasError,
    valueChangeHandler: addressChangeHandler,
    inputBlurHandler: addressBlurHandler,
    reset: addressResetHandler,
  } = useInput("", (value) => {
    return true;
  });

  const {
    value: enteredPassword,
    isValid: enteredPasswordIsValid,
    hasError: passwordInputHasError,
    valueChangeHandler: passwordChangeHandler,
    inputBlurHandler: passwordBlurHandler,
    reset: passwordResetHandler,
  } = useInput("", (value) => {
    return value.length >= 4;
  });

  const {
    value: enteredConfirmPassword,
    isValid: enteredConfirmPasswordIsValid,
    hasError: confirmPasswordInputHasError,
    valueChangeHandler: confirmPasswordChangeHandler,
    inputBlurHandler: confirmPasswordBlurHandler,
    reset: confirmPasswordResetHandler,
  } = useInput("", (value) => {
    return value.length >= 4 && value === enteredPassword;
  });

  const formIsValid =
    enteredEmailIsValid &&
    enteredFirstNameIsValid &&
    enteredLastNameIsValid &&
    enteredGenderIsValid &&
    enteredAddressIsValid &&
    enteredPasswordIsValid &&
    enteredConfirmPasswordIsValid;

  const resetForm = () => {
    emailResetHandler();
    firstNameResetHandler();
    lastNameResetHandler();
    genderResetHandler();
    addressResetHandler();
    passwordResetHandler();
    confirmPasswordResetHandler();
  };

  const submitHandler = (event) => {
    event.preventDefault();
    const response = userCtx.addNewUser(
      { entryType: "Email", value: enteredEmail, isValid: enteredEmailIsValid },
      {
        entryType: "FirstName",
        value: enteredFirstName,
        isValid: enteredFirstNameIsValid,
      },
      {
        entryType: "LastName",
        value: enteredLastName,
        isValid: enteredLastNameIsValid,
      },
      {
        entryType: "Gender",
        value: enteredGender,
        isValid: enteredGenderIsValid,
      },
      {
        entryType: "Address",
        value: enteredAddress,
        isValid: enteredAddressIsValid,
      },
      {
        entryType: "Password",
        value: enteredPassword,
        isValid: enteredPasswordIsValid,
      }
    );
    if (response.status === "Error") {
      setSignupModal((prevState) => {
        return { ...prevState, state: true, data: response };
      });
    }

    console.log(response);

    if (response.status === "Ok") {
      navigate("/tasks");
    }
    resetForm();
  };

  return (
    <Fragment>
      {signupModal.state && (
        <ErrorModal
          closeModal={closeSignupModal}
          openModal={signupModal}
        ></ErrorModal>
      )}
      <form onSubmit={submitHandler}>
        <Stack
          direction="column"
          justifyContent="center"
          alignItems="stretch"
          spacing={2}
        >
          <h2>Sign Up</h2>
          <TextField
            id="email"
            label="Email"
            value={enteredEmail}
            onChange={emailChangeHandler}
            onBlur={emailBlurHandler}
            error={emailInputHasError}
            focused={emailInputHasError}
            helperText={
              emailInputHasError
                ? "Please enter email in format xyz@zs.com"
                : ""
            }
            fullWidth
          />
          <Stack
            direction="row"
            justifyContent="center"
            alignItems="center"
            spacing={2}
          >
            <TextField
              id="firstName"
              label="First Name"
              value={enteredFirstName}
              onChange={firstNameChangeHandler}
              onBlur={firstNameBlurHandler}
              error={firstNameInputHasError}
              focused={firstNameInputHasError}
              fullWidth
            />
            <TextField
              id="lastName"
              label="Last Name"
              value={enteredLastName}
              onChange={lastNameChangeHandler}
              onBlur={lastNameBlurHandler}
              error={lastNameInputHasError}
              focused={lastNameInputHasError}
              fullWidth
            />
          </Stack>
          <FormControl fullWidth>
            <InputLabel id="ageSelectLabel">Gender</InputLabel>
            <Select
              labelId="ageSelectLabel"
              id="ageSelect"
              label="Gender"
              value={enteredGender}
              onChange={genderChangeHandler}
              onBlur={genderBlurHandler}
              error={genderInputHasError}
            >
              <MenuItem value={"male"}>Male</MenuItem>
              <MenuItem value={"female"}>Female</MenuItem>
              <MenuItem value={"other"}>Other</MenuItem>
              <MenuItem value={"none"}>None</MenuItem>
            </Select>
          </FormControl>
          <TextField
            id="address"
            label="Address"
            multiline
            rows={2}
            value={enteredAddress}
            onChange={addressChangeHandler}
            onBlur={addressBlurHandler}
            error={addressInputHasError}
            focused={addressInputHasError}
            fullWidth
          />
          <Stack
            direction="row"
            justifyContent="center"
            alignItems="center"
            spacing={2}
          >
            <TextField
              id="password"
              label="Password"
              type="password"
              value={enteredPassword}
              onChange={passwordChangeHandler}
              onBlur={passwordBlurHandler}
              error={passwordInputHasError}
              focused={passwordInputHasError}
              helperText={
                passwordInputHasError
                  ? "Password must be atleast 4 characters long"
                  : ""
              }
              fullWidth
            ></TextField>
            <TextField
              id="confirmPassword"
              label="Confirm Password"
              type="password"
              value={enteredConfirmPassword}
              onChange={confirmPasswordChangeHandler}
              onBlur={confirmPasswordBlurHandler}
              error={confirmPasswordInputHasError}
              focused={confirmPasswordInputHasError}
              helperText={
                confirmPasswordInputHasError
                  ? "Confirm password should match the password"
                  : ""
              }
              fullWidth
            ></TextField>
          </Stack>
          <Button
            variant="contained"
            style={{
              backgroundColor: "#004DC8",
              color: "#ffffff",
            }}
            disabled={!formIsValid}
            type="submit"
          >
            Sign Up
          </Button>
        </Stack>

        {/* <Grid item xs={12}>
        <Box
          component="img"
          sx={{
            height: 233,
            width: 350,
            maxHeight: { xs: 233, md: 167 },
            maxWidth: { xs: 350, md: 250 },
          }}
          alt="The house from the offer."
          src="https://images.unsplash.com/photo-1512917774080-9991f1c4c750?auto=format&w=350&dpr=2"
        />
      </Grid> */}
      </form>
    </Fragment>
  );
};

export default Signup;
