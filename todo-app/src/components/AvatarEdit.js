import { Grid, Badge, Avatar } from "@mui/material";
import React, { useContext, useRef, useState } from "react";
import { Fragment } from "react";
import AddCircleIcon from "@mui/icons-material/AddCircle";
import ArrowCircleLeftIcon from "@mui/icons-material/ArrowCircleLeft";
import UserContext from "store/UserContext";
import { useNavigate } from "react-router-dom";

const AvatarEdit = () => {
  const userCtx = useContext(UserContext);
  const uploadInputRef = useRef(null);
  const [profileImage, setProfileImage] = useState(userCtx.user.profileImage);
  const navigate = useNavigate();

  const onImageChange = (event) => {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = () => {
        setProfileImage(reader.result);
        userCtx.updateProfileImage(reader.result);
      };
    }
  };
  return (
    <Fragment>
      <Grid item xs={12}>
        <div style={{ position: "relative", display: "inline-block" }}>
          <ArrowCircleLeftIcon
            style={{
              position: "absolute",
              left: -70,
              top: 15,
              width: 60,
              height: 60,
              color: "#004DC8",
              cursor: "pointer",
            }}
            onClick={() => {
              navigate("/tasks");
            }}
          ></ArrowCircleLeftIcon>
          <h1>Profile</h1>
        </div>
      </Grid>
      <Grid item xs={12}>
        <input
          ref={uploadInputRef}
          type="file"
          accept="image/*"
          style={{ display: "none" }}
          onChange={onImageChange}
        />
        <Badge
          anchorOrigin={{ vertical: "bottom", horizontal: "right" }}
          badgeContent={
            <AddCircleIcon
              style={{
                color: "#004DC8",
                cursor: "pointer",
              }}
              sx={{ width: 50, height: 50 }}
              onClick={() =>
                uploadInputRef.current && uploadInputRef.current.click()
              }
            ></AddCircleIcon>
          }
        >
          <Avatar
            alt="Travis Howard"
            src={profileImage}
            sx={{ width: 150, height: 150 }}
            style={{ backgroundColor: "#21B1CC" }}
          >
            <h1>{userCtx.user.firstName[0] + userCtx.user.lastName[0]}</h1>
          </Avatar>
        </Badge>
      </Grid>
      <Grid item xs={12}>
        <br></br>
        <h3>{userCtx.user.firstName + " " + userCtx.user.lastName}</h3>
      </Grid>
    </Fragment>
  );
};

export default AvatarEdit;
