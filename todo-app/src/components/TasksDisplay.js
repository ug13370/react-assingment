import { Button, ButtonGroup, Divider, Grid, Stack } from "@mui/material";
import EditTaskModal from "modals/EditTaskModal";
import SurityModal from "modals/SurityModal";
import React, { useContext, useState } from "react";
import { Fragment } from "react";
import TaskContext from "store/TaskContext";
import filterResetSubject from "subjects/filterResetSubject";
import TaskItem from "./TaskItem";

const TasksDisplay = () => {
  const taskCtx = useContext(TaskContext);
  console.log(taskCtx.tasksToDisplay);

  const defaultEditTaskModal = { state: false, data: {} };
  const [editTaskModal, setEditTaskModal] = useState(defaultEditTaskModal);
  const closeEditTaskModal = () => {
    setEditTaskModal(defaultEditTaskModal);
  };

  const defaultSurityModal = { state: false, data: {} };
  const [surityModal, setSurityModal] = useState(defaultSurityModal);
  const closeSurityModal = () => {
    setSurityModal(defaultSurityModal);
    taskCtx.resetDeleteMode();
    taskCtx.resetDeleteList();
  };
  const surityModalConfirmed = () => {
    taskCtx.deleteTasks();
    setSurityModal(defaultSurityModal);
  };
  const setDeleteTaskModal = () => {
    setSurityModal((prevState) => {
      return {
        ...prevState,
        state: true,
        data: {
          action: "Delete confirmation",
          message: "Are you sure you want to delete the selected tasks?",
        },
      };
    });
  };

  const deleteMode = taskCtx.deleteMode;

  const createTask = () => {
    setEditTaskModal((prevState) => {
      return {
        ...prevState,
        state: true,
        data: { editMode: false, task: null },
      };
    });
  };

  const showAllTasks = () => {
    taskCtx.resetTasksToDisplay();
    filterResetSubject.next();
  };
  const cancelDelete = () => {
    taskCtx.resetDeleteMode();
    taskCtx.resetDeleteList();
  };

  return (
    <Fragment>
      {editTaskModal.state && (
        <EditTaskModal
          closeModal={closeEditTaskModal}
          openModal={editTaskModal}
        ></EditTaskModal>
      )}
      {surityModal.state && (
        <SurityModal
          closeModal={closeSurityModal}
          openModal={surityModal}
          actionConfirmed={surityModalConfirmed}
        ></SurityModal>
      )}
      <Grid container>
        <Grid item xs={12}>
          <Grid
            item
            xs={12}
            container
            direction="row"
            justifyContent="flex-start"
            alignItems="center"
          >
            <Grid item xs={6}>
              <h2>All Tasks</h2>
            </Grid>
            <Grid item xs={6}>
              <Stack
                direction="row"
                justifyContent="flex-end"
                alignItems="center"
                spacing={2}
              >
                <ButtonGroup variant="contained">
                  {!deleteMode && (
                    <Button
                      style={{
                        backgroundColor: "#004DC8",
                        color: "#ffffff",
                        fontSize: 13,
                      }}
                      onClick={showAllTasks}
                    >
                      All Tasks
                    </Button>
                  )}
                  {!deleteMode && (
                    <Button
                      style={{
                        backgroundColor: "#004DC8",
                        color: "#ffffff",
                        fontSize: 13,
                      }}
                      onClick={createTask}
                    >
                      Create Task
                    </Button>
                  )}
                  {!deleteMode && (
                    <Button
                      style={{
                        backgroundColor: "#004DC8",
                        color: "#ffffff",
                        fontSize: 13,
                      }}
                      onClick={() => {
                        taskCtx.setDeleteMode();
                      }}
                    >
                      Delete Tasks
                    </Button>
                  )}
                </ButtonGroup>

                {deleteMode && (
                  <Button
                    variant="contained"
                    style={{
                      backgroundColor: "#004DC8",
                      color: "#ffffff",
                      marginRight: "1%",
                    }}
                    onClick={setDeleteTaskModal}
                  >
                    Delete
                  </Button>
                )}
                {deleteMode && (
                  <Button
                    variant="outlined"
                    style={{
                      color: "#004DC8",
                      marginLeft: "1%",
                    }}
                    onClick={cancelDelete}
                  >
                    Cancel
                  </Button>
                )}
              </Stack>
            </Grid>
          </Grid>
          <Divider></Divider>
        </Grid>
        <Grid item xs={12} style={{ paddingTop: "2%" }}>
          <Stack
            direction="column"
            justifyContent="center"
            alignItems="stretch"
            spacing={2}
          >
            {taskCtx.tasksToDisplay.length !== 0 &&
              taskCtx.tasksToDisplay.map((task) => {
                return <TaskItem key={task.id} task={task}></TaskItem>;
              })}

            <Stack
              direction="row"
              justifyContent="center"
              alignItems="center"
              spacing={2}
            >
              {taskCtx.tasksToDisplay.length === 0 && <p>No tasks available</p>}
            </Stack>
          </Stack>
        </Grid>
      </Grid>
    </Fragment>
  );
};

export default TasksDisplay;
