import { LocalizationProvider, DatePicker } from "@mui/lab";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import {
  Button,
  Checkbox,
  FormControl,
  Grid,
  InputLabel,
  ListItemText,
  MenuItem,
  OutlinedInput,
  Select,
  Stack,
  TextField,
} from "@mui/material";
import useDateInput from "hooks/use-dateInput";
import React, { useContext, useState } from "react";
import useCheckboxesSelect from "hooks/use-checkboxesSelect";
import TaskContext from "store/TaskContext";
import filterResetSubject from "subjects/filterResetSubject";

const taskTypeNames = ["Completed Tasks", "Due Tasks", "Overdue Tasks"];
const taskCategoryNames = [
  "Meeting",
  "Chores",
  "Learning",
  "Reading",
  "Next Week",
];
const sortTypes = [
  "Sort by name (Asc)",
  "Sort by name (Desc)",
  "Sort by due date (Asc)",
  "Sort by due date (Desc)",
];

const Filters = () => {
  const taskCtx = useContext(TaskContext);

  const {
    value: enteredTaskTypes,
    isValid: enteredTaskTypesIsValid,
    selectAll: selectAllTaskTypes,
    hasError: taskTypesHasError,
    valueChangeHandler: taskTypesChangeHandler,
    inputBlurHandler: taskTypesBlurHandler,
    reset: taskTypesReset,
  } = useCheckboxesSelect([], taskTypeNames, (value) => {
    return value.length !== 0;
  });

  const {
    value: enteredTaskCategories,
    isValid: enteredTaskCategoriesIsValid,
    selectAll: selectAllTaskCategories,
    hasError: taskCategoriesHasError,
    valueChangeHandler: taskCategoriesChangeHandler,
    inputBlurHandler: taskCategoriesBlurHandler,
    reset: taskCategoriesReset,
  } = useCheckboxesSelect([], taskCategoryNames, (value) => {
    return value.length !== 0;
  });

  const [sortName, setSortName] = useState("");
  const handleChangeInSort = (event) => {
    const {
      target: { value },
    } = event;
    setSortName(value);
  };
  const sortNameReset = () => {
    setSortName("");
  };

  const {
    value: enteredStartDate,
    isValid: enteredStartDateIsValid,
    hasError: startDateInputHasError,
    valueChangeHandler: startDateChangeHandler,
    inputBlurHandler: startDateBlurHandler,
    reset: startDateResetHandler,
    minVal: startMinDate,
  } = useDateInput(null, (value) => {
    return true;
  });

  const {
    value: enteredEndDate,
    isValid: enteredEndDateIsValid,
    hasError: endDateInputHasError,
    valueChangeHandler: endDateChangeHandler,
    inputBlurHandler: endDateBlurHandler,
    reset: endDateResetHandler,
    minVal: endMinDate,
  } = useDateInput(null, (value) => {
    return true;
  });

  const filterFormIsValid =
    enteredEndDateIsValid &&
    enteredTaskTypesIsValid &&
    enteredStartDateIsValid &&
    enteredTaskCategoriesIsValid;

  const resetFilter = () => {
    taskCtx.resetTasksToDisplay();
    taskTypesReset();
    taskCategoriesReset();
    sortNameReset();
    startDateResetHandler();
    endDateResetHandler();
  };

  const applyFilter = () => {
    console.log(
      enteredTaskTypes,
      enteredTaskCategories,
      enteredStartDate,
      enteredEndDate,
      sortName
    );

    taskCtx.applyFilter(
      enteredTaskTypes,
      enteredTaskCategories,
      enteredStartDate,
      enteredEndDate,
      sortName
    );
  };

  filterResetSubject.subscribe(() => {
    taskTypesReset();
    taskCategoriesReset();
    sortNameReset();
    startDateResetHandler();
    endDateResetHandler();
  });

  return (
    <form>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <h1>Filters :-</h1>
        </Grid>

        <Grid item xs={12}>
          <FormControl fullWidth>
            <InputLabel id="taskTypeLabel">Task Types</InputLabel>
            <Select
              labelId="taskTypeLabel"
              id="taskTypesSelect"
              multiple
              value={enteredTaskTypes}
              onChange={taskTypesChangeHandler}
              input={<OutlinedInput label="Task Types" />}
              renderValue={(selected) => selected.join(", ")}
              onBlur={taskTypesBlurHandler}
            >
              {
                <MenuItem key={"Select all"} value={"Select all"}>
                  <Checkbox checked={selectAllTaskTypes}></Checkbox>
                  <ListItemText primary={"Select all"}></ListItemText>
                </MenuItem>
              }
              {taskTypeNames.map((name) => (
                <MenuItem key={name} value={name}>
                  <Checkbox checked={enteredTaskTypes.indexOf(name) > -1} />
                  <ListItemText primary={name} />
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>

        <Grid item xs={12}>
          <FormControl fullWidth>
            <InputLabel id="taskCategoriesLabel">Task Categories</InputLabel>
            <Select
              labelId="taskCategoriesLabel"
              id="taskCategoriesSelect"
              multiple
              value={enteredTaskCategories}
              onChange={taskCategoriesChangeHandler}
              input={<OutlinedInput label="Task Categories" />}
              renderValue={(selected) => selected.join(", ")}
              onBlur={taskCategoriesBlurHandler}
            >
              {
                <MenuItem key={"Select all"} value={"Select all"}>
                  <Checkbox checked={selectAllTaskCategories}></Checkbox>
                  <ListItemText primary={"Select all"}></ListItemText>
                </MenuItem>
              }
              {taskCategoryNames.map((name) => (
                <MenuItem key={name} value={name}>
                  <Checkbox
                    checked={enteredTaskCategories.indexOf(name) > -1}
                  />
                  <ListItemText primary={name} />
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>

        <Grid item xs={12}>
          <Stack
            direction="row"
            justifyContent="flex-start"
            alignItems="center"
            spacing={2}
          >
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DatePicker
                label="Start date"
                inputFormat="dd/MM/yyyy"
                value={enteredStartDate}
                onChange={startDateChangeHandler}
                maxDate={enteredEndDate !== "" ? enteredEndDate : null}
                clearable
                renderInput={(params) => <TextField {...params} />}
              />
            </LocalizationProvider>
            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DatePicker
                label="End date"
                inputFormat="dd/MM/yyyy"
                value={enteredEndDate}
                onChange={endDateChangeHandler}
                minDate={enteredStartDate !== "" ? enteredStartDate : null}
                clearable
                renderInput={(params) => <TextField {...params} />}
              />
            </LocalizationProvider>
          </Stack>
        </Grid>

        <Grid item xs={12}>
          <FormControl fullWidth>
            <InputLabel id="sortSelectLabel">Sort</InputLabel>
            <Select
              labelId="sortSelectLabel"
              id="sortSelect"
              value={sortName}
              label="Sort"
              onChange={handleChangeInSort}
            >
              {sortTypes.map((name) => (
                <MenuItem key={name} value={name}>
                  {name}
                </MenuItem>
              ))}
            </Select>
          </FormControl>
        </Grid>

        <Grid item xs={12}>
          <Button
            variant="contained"
            style={{
              backgroundColor: "#004DC8",
              color: "#ffffff",
              marginRight: "1%",
            }}
            onClick={applyFilter}
          >
            Apply
          </Button>

          <Button
            variant="outlined"
            style={{
              color: "#004DC8",
              marginLeft: "1%",
            }}
            onClick={resetFilter}
          >
            Reset
          </Button>
        </Grid>
      </Grid>
    </form>
  );
};

export default Filters;
