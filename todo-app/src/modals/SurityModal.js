import { Button, Modal, Paper, Stack } from "@mui/material";
import React, { useState } from "react";
import { Fragment } from "react";
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  borderRadius: 7,
  height: "auto",
  width: "30vw",
  p: 4,
};
const SurityModal = (props) => {
  const [open, setOpen] = useState(props.openModal.state);
  const handleClose = () => {
    props.closeModal();
    setOpen(false);
  };
  const handleYes =()=>{
    props.actionConfirmed();
    setOpen(false);
  }
  return (
    <Fragment>
      <Modal open={open} onClose={handleClose}>
        <Paper elevation={10} sx={style}>
          <Stack
            direction="column"
            justifyContent="center"
            alignItems="stretch"
            spacing={1.5}
          >
            <h1 style={{ marginBottom: "0%" }}>
              {props.openModal.data.action}
            </h1>
            <p>{props.openModal.data.message}</p>
            <Stack
              direction="row"
              justifyContent="flex-end"
              alignItems="center"
              spacing={2}
            >
              <Button
                variant="contained"
                style={{
                  backgroundColor: "#004DC8",
                  color: "#ffffff",
                }}
                type="button"
                onClick={handleYes}
              >
                Yes
              </Button>
              <Button
                variant="outlined"
                style={{
                  color: "#004DC8",
                }}
                type="button"
                onClick={handleClose}
              >
                Cancel
              </Button>
            </Stack>
          </Stack>
        </Paper>
      </Modal>
    </Fragment>
  );
};
export default SurityModal;
