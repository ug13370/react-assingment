import { DesktopDatePicker, LocalizationProvider } from "@mui/lab";
import { AdapterDateFns } from "@mui/x-date-pickers/AdapterDateFns";
import {
  Box,
  Button,
  Checkbox,
  FormControl,
  FormControlLabel,
  FormHelperText,
  InputLabel,
  ListItemText,
  MenuItem,
  Modal,
  OutlinedInput,
  Paper,
  Select,
  Stack,
  TextField,
} from "@mui/material";
import React, { Fragment, useContext, useRef, useState } from "react";
import useInput from "hooks/use-input";
import useDateInput from "hooks/use-dateInput";
import useCheckboxesSelect from "hooks/use-checkboxesSelect";
import AddIcon from "@mui/icons-material/Add";
import TaskContext from "store/TaskContext";

const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  borderRadius: 7,
  height: "auto",
  width: "40vw",
  p: 4,
};
const taskCategoryNames = [
  "Meeting",
  "Chores",
  "Learning",
  "Reading",
  "Next Week",
];

const EditTaskModal = (props) => {
  const taskCtx = useContext(TaskContext);
  const [open, setOpen] = useState(props.openModal.state);
  const editMode = props.openModal.data.editMode;
  const task = props.openModal.data.task;
  const handleClose = () => {
    props.closeModal();
    setOpen(false);
  };

  const defaultEditTaskFormValues = {
    publicStatus: editMode ? task.publicStatus : false,
    doneStatus: editMode ? task.doneStatus : false,
    taskImage: editMode ? task.taskImage : null,
    taskName: editMode ? task.taskName : "",
    dueDate: editMode ? task.dueDate : new Date(),
    taskCategories: editMode ? task.taskCategories : [],
  };

  const [publicStatus, setPublicStatus] = useState(
    defaultEditTaskFormValues.publicStatus
  );
  const changePublicStatus = () => {
    setPublicStatus((prevState) => {
      return !prevState;
    });
  };

  const [doneStatus, setDoneStatus] = useState(
    defaultEditTaskFormValues.doneStatus
  );
  const changeDoneStatus = () => {
    setDoneStatus((prevState) => {
      return !prevState;
    });
  };

  const uploadInputRef = useRef(null);
  const [taskImage, setTaskImage] = useState(
    defaultEditTaskFormValues.taskImage
  );

  const {
    value: enteredTaskName,
    isValid: enteredTaskNameIsValid,
    hasError: taskNameInputHasError,
    valueChangeHandler: taskNameChangeHandler,
    inputBlurHandler: taskNameBlurHandler,
    reset: taskNameResetHandler,
  } = useInput(defaultEditTaskFormValues.taskName, (value) => {
    return value !== "";
  });

  const {
    value: enteredDueDate,
    isValid: enteredDueDateIsValid,
    hasError: dueDateInputHasError,
    valueChangeHandler: dueDateChangeHandler,
    inputBlurHandler: dueDateBlurHandler,
    reset: dueDateResetHandler,
    minVal: minDate,
  } = useDateInput(defaultEditTaskFormValues.dueDate, (value) => {
    return value !== "";
  });

  const {
    value: enteredTaskCategories,
    isValid: enteredTaskCategoriesIsValid,
    selectAll: selectAllTaskCategories,
    hasError: taskCategoriesHasError,
    valueChangeHandler: taskCategoriesChangeHandler,
    inputBlurHandler: taskCategoriesBlurHandler,
    reset: taskCategoriesReset,
  } = useCheckboxesSelect(
    defaultEditTaskFormValues.taskCategories,
    taskCategoryNames,
    (value) => {
      return value.length !== 0;
    }
  );

  const onImageChange = (event) => {
    const reader = new FileReader();

    if (event.target.files && event.target.files.length) {
      const [file] = event.target.files;
      reader.readAsDataURL(file);

      reader.onload = () => {
        setTaskImage(reader.result);
      };
    }
  };

  const editTaskFormIsValid =
    enteredDueDateIsValid &&
    enteredTaskNameIsValid &&
    enteredTaskCategoriesIsValid;

  const touchAllFields = () => {
    taskNameBlurHandler();
    dueDateBlurHandler();
    taskCategoriesBlurHandler();
  };

  const onCreate = (event) => {
    event.preventDefault();
    if (editTaskFormIsValid) {
      const response = taskCtx.addNewTask(
        {
          value: enteredTaskName,
          isValid: enteredTaskNameIsValid,
        },
        {
          value: enteredDueDate,
          isValid: enteredDueDateIsValid,
        },
        {
          value: enteredTaskCategories,
          isValid: enteredDueDateIsValid,
        },
        {
          value: taskImage,
        },
        {
          value: publicStatus,
        },
        {
          value: doneStatus,
        }
      );
      console.log(response);
      handleClose();
    } else {
      touchAllFields();
    }
  };

  const onEdit = (event) => {
    event.preventDefault();
    if (editTaskFormIsValid) {
      const response = taskCtx.updateTask(
        { value: task.id },
        {
          value: enteredTaskName,
          isValid: enteredTaskNameIsValid,
        },
        {
          value: enteredDueDate,
          isValid: enteredDueDateIsValid,
        },
        {
          value: enteredTaskCategories,
          isValid: enteredDueDateIsValid,
        },
        {
          value: taskImage,
        },
        {
          value: publicStatus,
        },
        {
          value: doneStatus,
        }
      );
      console.log(response);
      handleClose();
    } else {
      touchAllFields();
    }
  };

  return (
    <Fragment>
      <Modal open={open} onClose={handleClose}>
        <Paper elevation={10} sx={style}>
          <Stack
            direction="column"
            justifyContent="center"
            alignItems="stretch"
            spacing={2}
          >
            <h2>{!editMode ? "Create a new task" : "Edit task"}</h2>
            <TextField
              id="taskName"
              label="Task Name"
              value={enteredTaskName}
              onChange={taskNameChangeHandler}
              onBlur={taskNameBlurHandler}
              error={taskNameInputHasError}
              focused={taskNameInputHasError}
              helperText={
                taskNameInputHasError ? "Task name should not be empty" : ""
              }
              fullWidth
            />

            <LocalizationProvider dateAdapter={AdapterDateFns}>
              <DesktopDatePicker
                label="Due date"
                inputFormat="MM/dd/yyyy"
                value={enteredDueDate}
                onChange={dueDateChangeHandler}
                minDate={minDate}
                renderInput={(params) => <TextField {...params} />}
              />
            </LocalizationProvider>

            <FormControl fullWidth focused={taskCategoriesHasError}>
              <InputLabel
                id="taskCategoriesLabel"
                error={taskCategoriesHasError}
              >
                Task Categories
              </InputLabel>
              <Select
                labelId="taskCategoriesLabel"
                id="taskCategoriesSelect"
                multiple
                value={enteredTaskCategories}
                onChange={taskCategoriesChangeHandler}
                input={<OutlinedInput label="Task Categories" />}
                renderValue={(selected) => selected.join(", ")}
                error={taskCategoriesHasError}
                onBlur={taskCategoriesBlurHandler}
              >
                {
                  <MenuItem key={"Select all"} value={"Select all"}>
                    <Checkbox checked={selectAllTaskCategories}></Checkbox>
                    <ListItemText primary={"Select all"}></ListItemText>
                  </MenuItem>
                }
                {taskCategoryNames.map((name) => (
                  <MenuItem key={name} value={name}>
                    <Checkbox
                      checked={enteredTaskCategories.indexOf(name) > -1}
                    />
                    <ListItemText primary={name} />
                  </MenuItem>
                ))}
              </Select>
              {taskCategoriesHasError && (
                <FormHelperText error={taskCategoriesHasError}>
                  Please select atleast 1 category.
                </FormHelperText>
              )}
            </FormControl>

            <Stack
              direction="column"
              justifyContent="center"
              alignItems="flex-start"
              spacing={2}
            >
              <input
                ref={uploadInputRef}
                type="file"
                accept="image/*"
                style={{ display: "none" }}
                onChange={onImageChange}
              />
              <Button
                variant="contained"
                style={{
                  backgroundColor: "#004DC8",
                  color: "#ffffff",
                }}
                onClick={() =>
                  uploadInputRef.current && uploadInputRef.current.click()
                }
                startIcon={<AddIcon />}
              >
                Task Image
              </Button>
              {taskImage !== null && (
                <Box
                  component="img"
                  sx={{
                    height: "auto",
                    width: 200,
                  }}
                  alt="Task Image"
                  src={taskImage}
                />
              )}
            </Stack>

            <Stack
              direction="row"
              justifyContent="flex-start"
              alignItems="center"
              spacing={2}
            >
              <FormControlLabel
                label="Make this task public"
                control={
                  <Checkbox
                    sx={{
                      color: "#004DC8",
                      "&.Mui-checked": {
                        color: "#004DC8",
                      },
                    }}
                    checked={publicStatus}
                    onChange={changePublicStatus}
                  />
                }
              />
              <FormControlLabel
                label="Mark as done"
                control={
                  <Checkbox
                    sx={{
                      color: "#004DC8",
                      "&.Mui-checked": {
                        color: "#004DC8",
                      },
                    }}
                    checked={doneStatus}
                    onChange={changeDoneStatus}
                  />
                }
              />
            </Stack>

            <Stack
              direction="row"
              justifyContent="flex-start"
              alignItems="center"
              spacing={2}
            >
              {!editMode && (
                <Button
                  variant="contained"
                  style={{
                    backgroundColor: "#004DC8",
                    color: "#ffffff",
                    marginRight: "1%",
                  }}
                  onClick={onCreate}
                >
                  Create
                </Button>
              )}

              {editMode && (
                <Button
                  variant="contained"
                  style={{
                    backgroundColor: "#004DC8",
                    color: "#ffffff",
                    marginRight: "1%",
                  }}
                  onClick={onEdit}
                >
                  Save
                </Button>
              )}

              <Button
                variant="outlined"
                style={{
                  color: "#004DC8",
                  marginLeft: "1%",
                }}
                onClick={handleClose}
              >
                Cancel
              </Button>
            </Stack>
          </Stack>
        </Paper>
      </Modal>
    </Fragment>
  );
};

export default EditTaskModal;
