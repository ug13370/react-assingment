import { Button, Modal, Paper, Stack } from "@mui/material";
import React, { useState } from "react";
import { Fragment } from "react";
const style = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  borderRadius: 7,
  height: "auto",
  width: "30vw",
  p: 4,
};
const ErrorModal = (props) => {
  const [open, setOpen] = useState(props.openModal.state);
  const handleClose = () => {
    props.closeModal();
    setOpen(false);
  };
  return (
    <Fragment>
      <Modal open={open} onClose={handleClose}>
        <Paper elevation={10} sx={style}>
          <Stack
            direction="column"
            justifyContent="center"
            alignItems="stretch"
            spacing={1.5}
          >
            <h1 style={{ marginBottom: "0%" }}>
              {props.openModal.data.status}
            </h1>
            <p>{props.openModal.data.message}</p>
            <p>{props.openModal.data.helperMessage}</p>
            <Stack
              direction="row"
              justifyContent="flex-end"
              alignItems="center"
              spacing={2}
            >
              <Button
                variant="contained"
                style={{
                  backgroundColor: "#004DC8",
                  color: "#ffffff",
                }}
                type="button"
                onClick={handleClose}
              >
                Ok
              </Button>
              <Button
                variant="outlined"
                style={{
                  color: "#004DC8",
                }}
                type="button"
                onClick={handleClose}
              >
                Cancel
              </Button>
            </Stack>
          </Stack>
        </Paper>
      </Modal>
    </Fragment>
  );
};
export default ErrorModal;
