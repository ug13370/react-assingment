import { Button, Grid, Paper, Stack } from "@mui/material";
import Signin from "components/Signin";
import Signup from "components/Signup";
import React, { useEffect, useState } from "react";
import { Fragment } from "react";

import Img from "../assets/pattern.png";

const Authenticate = () => {
  const [signupMode, setSignupMode] = useState(true);
  const switchSignMode = () => {
    setSignupMode(!signupMode);
  };
  return (
    <Fragment>
      <Grid style={{ height: "4%" }}></Grid>
      <Stack
        direction="row"
        justifyContent="center"
        alignItems="center"
        spacing={2}
        height="100vh"
      >
        <Paper
          elevation={10}
          style={{ borderRadius: 15, height: "80vh", width: "70vw" }}
        >
          <Stack
            direction="row"
            justifyContent="flex-start"
            alignItems="center"
            spacing={0}
            height="100%"
          >
            <div
              style={{
                backgroundImage: `url(${Img})`,
                height: "100%",
                width: "60%",
                borderRadius: 15,
              }}
            >
              <Stack
                direction="column"
                justifyContent="center"
                alignItems="center"
                spacing={2}
                height="100%"
              >
                <h1
                  style={{
                    color: "#ffffff",
                    paddingRight: "5%",
                    paddingLeft: "5%",
                    textAlign: "center",
                  }}
                >
                  Welcome Back!
                </h1>
                <h5
                  style={{
                    color: "#ffffff",
                    paddingRight: "5%",
                    paddingLeft: "5%",
                    textAlign: "center",
                  }}
                >
                  {signupMode
                    ? "To keep connected with us please log in with your personal info"
                    : "Enter your personal details and start journey with us"}
                </h5>
                <Button
                  variant="outlined"
                  style={{
                    color: "#ffffff",
                    borderColor: "#ffffff",
                  }}
                  onClick={switchSignMode}
                >
                  {signupMode ? "Sign in" : "Sign up"}
                </Button>
              </Stack>
            </div>
            <div style={{ padding: "5%", width: "40%" }}>
              {signupMode && <Signup></Signup>}
              {!signupMode && <Signin></Signin>}
            </div>
          </Stack>
        </Paper>
      </Stack>
    </Fragment>
  );
};

export default Authenticate;
