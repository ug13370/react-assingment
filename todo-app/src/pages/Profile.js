import { Grid, Paper } from "@mui/material";
import React from "react";
import { Fragment } from "react";
import UserDetails from "components/UserDetails";
import AvatarEdit from "components/AvatarEdit";
import Header from "components/Header";
const Profile = () => {
  return (
    <Fragment>
      <Header></Header>
      <Grid container>
        <Grid item xs={1}></Grid>
        <Grid item xs={3}>
          <Grid
            container
            item
            direction="column"
            alignItems="center"
            justifyContent="center"
          >
            <AvatarEdit></AvatarEdit>
          </Grid>
        </Grid>
        <Grid item xs={7}>
          <Paper
            elevation={10}
            style={{ borderRadius: 15, height: 'auto', marginTop: "3%" }}
          >
            <div style={{ padding: "6%" }}>
              <UserDetails></UserDetails>
            </div>
          </Paper>
        </Grid>
        <Grid item xs={1}></Grid>
      </Grid>
    </Fragment>
  );
};

export default Profile;
