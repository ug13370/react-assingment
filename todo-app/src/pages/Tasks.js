import { Grid, Paper } from "@mui/material";
import Filters from "components/Filters";
import Header from "components/Header";
import TasksDisplay from "components/TasksDisplay";
import React from "react";
import { Fragment } from "react";

const Tasks = () => {
  return (
    <Fragment>
      <Header></Header>
      <Grid container>
        <Grid item xs={1}></Grid>
        <Grid item xs={3}>
          <Paper
            elevation={10}
            style={{ borderRadius: 15, height: "auto", marginTop: "10%" }}
          >
            <div style={{ padding: "6%" }}>
              <Filters></Filters>
            </div>
          </Paper>
        </Grid>
        <Grid item xs={7}>
          <div style={{ margin: "6%" }}>
            <TasksDisplay></TasksDisplay>
          </div>
        </Grid>
        <Grid item xs={1}></Grid>
      </Grid>
    </Fragment>
  );
};

export default Tasks;
