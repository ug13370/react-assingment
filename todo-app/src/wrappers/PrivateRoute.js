const React = require("react");
const { Navigate, Outlet } = require("react-router-dom");

const PrivateRoute = ({ auth: { isAuthenticated } }) => {
    console.log(isAuthenticated)
    return isAuthenticated ? <Outlet /> : <Navigate to="/authenticate" />;
  };

export default PrivateRoute;  