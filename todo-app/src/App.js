import Authenticate from "pages/Authenticate";
import Profile from "pages/Profile";
import Tasks from "pages/Tasks";
import React, { useContext } from "react";
import "./App.css";
import { Navigate, Route, Routes } from "react-router-dom";
import "react-date-range/dist/styles.css"; // main style file
import "react-date-range/dist/theme/default.css"; // theme css file
import UserContext from "store/UserContext";
import PrivateRoute from "wrappers/PrivateRoute";

function App() {
  if (localStorage.getItem("Users") === null)
    localStorage.setItem("Users", JSON.stringify([]));

  if (localStorage.getItem("PublicTasks") === null)
    localStorage.setItem("PublicTasks", JSON.stringify([]));

  const userCtx = useContext(UserContext);

  return (
    <Routes>
      <Route
        path="/"
        element={<Navigate replace to="/authenticate"></Navigate>}
      ></Route>
      <Route
        path="/authenticate"
        element={<Authenticate></Authenticate>}
      ></Route>
      <Route
        element={
          <PrivateRoute auth={{ isAuthenticated: userCtx.userIsLoggedIn }} />
        }
      >
        <Route path="/profile" element={<Profile></Profile>} />
      </Route>
      <Route
        element={
          <PrivateRoute auth={{ isAuthenticated: userCtx.userIsLoggedIn }} />
        }
      >
        <Route path="/tasks" element={<Tasks></Tasks>} />
      </Route>
      <Route
        path="*"
        element={<Navigate replace to="/authenticate"></Navigate>}
      ></Route>
    </Routes>
  );
}

export default App;
